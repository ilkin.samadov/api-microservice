package com.copaco.apimicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMicroserviceApplication {

	public static void main(String[] args) {
		System.out.println("hello world");
		SpringApplication.run(ApiMicroserviceApplication.class, args);
	}

}
