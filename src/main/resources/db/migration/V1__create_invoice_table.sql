CREATE TABLE IF NOT EXISTS invoice (
                         id SERIAL PRIMARY KEY,
                         invoice_number VARCHAR(20) NOT NULL,
                         customer_name VARCHAR(100) NOT NULL,
                         total_amount DECIMAL(10, 2) NOT NULL,
                         created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);