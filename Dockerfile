FROM openjdk:17-jdk
WORKDIR /app/
COPY build/libs/api-microservice-0.0.1.jar /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/api-microservice-0.0.1.jar"]
